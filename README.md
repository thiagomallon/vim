# Fast setup for Vim (no install) - Ubuntu
It has customized color themes, snippets and .vimrc file. The goal is to setting up easier and faster a development based Vim version by using a compiled version and all customization that works for me.

# Installation

```sh
$ apt install vim
$ mkdir ~/vim 
$ cd ~/vim
$ ./install.sh
```

# installed plugins
- pathogen
- nerdtree
- vim-go
- vim-composer
- vim-phpunit
- tlib_vim
- vim-addon-mw-utils
- vim-snipmate
- vim-node
- vim-javascript-syntax
- snipmate-nodejs
- jshint
- vim-blade
- vim-laravel
- vim-polyglot
- vim-twig

Have fun!
